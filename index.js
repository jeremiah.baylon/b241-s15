//alert("Hello");

//console.log("Hello World!");

// Syntax of console.log: console.log(variableOrStringToBeConsoled);

//It will comment parts of the code that gets ignored by the langauge

/*
	There are two types of comments
	1. the single line comment denoted by two slashes
	2. The multi-line comment denoted by slash and asterisk
*/

// [Section] Syntax, statements
	// Statements in programming are instructions that we tell the computer to perform
	//JS staements usually end with semicolon (;)
	//Semicolons are not required in JS but we will use it to help us train to locate where statement ends.
	// A syntax in programming, it is the set of rules that we describes statements must be constructed
	// All lines/blocs of code should be written in specific manner to work. This is due to hpw these codes where initially programmed to function in a certain manner.


// [Section] Variable
	//Variables are used to contain data
	// Any information that is used by an application is stored in what we call the memory
	// when we create variables, certain portions of device's memory is given a name that we call variables

	//This makes it easier for us to associate information stored in our devices to actual "names" information

	//Declaring Variables
	// Declaring Variables - it tells our devices that a varibale name is created and is ready to store data.
		// Syntax
			/*let/const variableName;*/

	let myVariable = "Ada Lovelace";
	let variable;
	//This will caused an undefined variable because the declared variable does not have initial value
	console.log(variable);
	//Const keyword is use when the value of the variable won't change.
	const constVariable = "John Doe";
	//console.log() is useful for printing values of variables or certain results of code into the browser's console.
	console.log(myVariable);

	/*
		Guides in writing Variables
			1. use the let keyword followed the variable name of your choose and use the assignment operatior (=) to assign value.
			2. Variable names should start with lowercase character, use camelCasing for the multiple words.
			3. for constant variables, use the 'const keyword'
				Note: If we use const keyword in declaring a variable, we cannot change the value of its variable.
			4. Variable names should be indicative(descriptive) of the value being stored to avoid confusion.

	*/


	// Declare and initialize
		// Initializing variables - the instance when a variable is given its firs/initial value
		// Syntax: 
			// let/conts variableName = initial value;
	//Declaratio and Initialization of the varibale occur
	/*console.log(productName);*/
	
	let productName = "desktop computer";
	console.log(productName);

	//Declaration
	let desktopName;
	console.log(desktopName);
	//Initialization of the vlaue of variable desktopName
	desktopName = "Dell";
	console.log(desktopName);

	// Reassigning value
		//Syntax: variableName = newValue
	productName = "Personal Computer";
	console.log(productName);

	const name = "Chris";
	console.log(name);
	// This reassignment will cause an error since we cannot change/reassign the initila value of constant variable.
	/*name = "Topher";
	console.log(name);*/

	// This will cause an error on our code because the productName variable is already taken.
	/*let productName = "Laptop";*/


	//var vs let/const
		//some of you may wonder why we used let and const keyweord in declaring a varibale when we search online, we usually see var.

		//var - is also used in declaring variable but var is an ecmaScript1 feature [(1997)].

	let lastName;
	lastName = "Mortel";
	console.log(lastName)

/*
	Using var, bad practice.
	batch = "Batch 241";

	var batch;
	console.log(batch);*/

/////////////////////////////